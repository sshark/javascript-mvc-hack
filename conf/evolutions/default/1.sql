# Tasks schema
 
# --- !Ups

CREATE SEQUENCE contact_id_seq;
CREATE TABLE contact (
    id integer NOT NULL DEFAULT nextval('contact_id_seq'),
    name varchar(255),
    email varchar(255),
    age integer,
    country varchar(255),
    city varchar(255),
    date_added timestamp
);
 
# --- !Downs
 
DROP TABLE contact;
DROP SEQUENCE contact_id_seq;
