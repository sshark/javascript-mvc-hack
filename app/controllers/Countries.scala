package controllers

import play.api.mvc.{Action, Controller}
import play.api.libs.json.Json

object Countries extends Controller {
	val countriesAndCities = Map(
		"Singapore" -> List("Singapore"),
		"Malaysia" -> List("Kuala Lumpur", "Penang", "Ipoh"),
		"Thailand" -> List("Bangkok", "Hat Yai"))

	def index = Action {	
		Ok(Json.toJson(countriesAndCities.keys))    	
    }

    def citiesOf(country : String) = Action {
    	Ok(Json.toJson(countriesAndCities.get(country)))	
    }
}
