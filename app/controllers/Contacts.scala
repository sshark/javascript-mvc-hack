package controllers

import play.api.mvc.{Action, Controller}
import play.api.libs.json._
import play.api.libs.json.Json._
import play.api.libs.json.Reads._
import play.api.libs.json.Writes._
import play.api.libs.functional.syntax._
import play.api.Play.current
import play.api.Logger

import java.util.Date

import models.Contact

import anorm._
import anorm.SqlParser._
import play.api.db.DB
import com.tindr.pusher.Pusher

object Contacts extends Controller {

  val contactsChannel = "contacts_channel"
  val newContactEvent = "contact_new"
  val NOBODY = "__nobody__";

  val contact = {
    get[Long]("id") ~
    get[String]("name") ~
    get[String]("email") ~
    get[Int]("age") ~
    get[String]("country") ~
    get[String]("city") ~
    get[Date]("date_added") map {
      case id ~ name ~ email ~ age ~ country ~ city ~ dateCreated => Contact(id, name, email, age, country, city, dateCreated)
    }
  }

  val contactReads : Reads[Contact] = (
    (__ \ "id").read[Long] and
    (__ \ "name").read[String] and
    (__ \ "email").read[String] and
    (__ \ "age").read[Int] and
    (__ \ "country").read[String] and
    (__ \ "city").read[String] and
    (__ \ "dateCreated").read[Date]
  )(Contact)

  val contactWrites : Writes[Contact] = (
    (__ \ "id").write[Long] and
    (__ \ "name").write[String] and
    (__ \ "email").write[String] and
    (__ \ "age").write[Int] and
    (__ \ "country").write[String] and
    (__ \ "city").write[String] and
    (__ \ "dateCreated").write[Date]
  )(unlift(Contact.unapply))

  implicit val contactFormat : Format[Contact] = Format(contactReads, contactWrites)

  def update(id : Long) = Action(parse.json) {request =>
    Logger.debug("Updating " + request.body)
    val json = request.body \ "contact"

    val count = DB.withConnection { implicit c =>
      SQL("update contact set name = {name}, email = {email}, age = {age}, country = {country}, city = {city}, date_added = {dateCreated} where id = {id}").on(
        'id -> id,
        'name -> (json \ "name").as[String],
        'email -> (json \ "email").as[String],
        'age -> (json \ "age").as[Int],
        'country -> (json \ "country").as[String],
        'city -> (json \ "city").as[String],
        'dateCreated -> new Date
      ).executeUpdate()
    }
    Logger.debug("Number of record updated is " + count)

    val socketId = request.headers.get("X-Pusher-Socket");

    Logger.debug("Saving new contact from " + socketId.getOrElse(NOBODY))

    Pusher().trigger(contactsChannel, newContactEvent,
      "{\"message\" : \"Updated row from " + socketId.getOrElse(NOBODY) + "\"}", socketId)

    Ok("{\"contact\" : {\"id\" : " + id + "," +
      "\"name\" :\"" + (json \ "name").as[String] + "\"," +
      "\"email\" : \"" + (json \ "email").as[String] + "\"," +
      "\"age\" : " + (json \ "age").as[Int] + "," +
      "\"country\" : \"" + (json \ "country").as[String] + "\"," +
      "\"city\" : \"" + (json \ "city").as[String] + "\"," +
      "\"dateCreated\" : \"" + new Date + "\"}}").as("application/json")
  }

  def save = Action(parse.json) { request =>
    Logger.debug("Saving " + request.body)
    val json = request.body \ "contact"

    val id = DB.withConnection { implicit c =>
      SQL("insert into contact (name, email, age, country, city, date_added) values ({name}, {email}, {age}, {country}, {city}, {dateCreated})").on(
        'name -> (json \ "name").as[String],
        'email -> (json \ "email").as[String],
        'age -> (json \ "age").as[Int],
        'country -> (json \ "country").as[String],
        'city -> (json \ "city").as[String],
        'dateCreated -> new Date
      ).executeInsert()
    } match {
      case Some(newId) => newId
      case None => None
    }
    Logger.debug("New contact has the id of " + id)

    val socketId = request.headers.get("X-Pusher-Socket")

    Logger.debug("Saving new contact from " + socketId.getOrElse(NOBODY))

    Pusher().trigger(contactsChannel, newContactEvent,
      "{\"message\" : \"Added new row from " + socketId.getOrElse(NOBODY) + "\"}", socketId)

    Ok("{\"contact\" : {\"id\" : " + id + "," +
      "\"name\" :\"" + (json \ "name").as[String] + "\"," +
      "\"email\" :\"" + (json \ "email").as[String] + "\"," +
      "\"age\" :\"" + (json \ "age").as[Int] + "\"," +
      "\"country\" : \"" + (json \ "country").as[String] + "\"," +
      "\"city\" : \"" + (json \ "city").as[String] + "\"," +
      "\"dateCreated\" : \"" + new Date + "\"}}").as("application/json")
  }

  def list = Action {request =>
    Logger.debug("List all contacts... for " + request.headers.get("X-Pusher-Socket").getOrElse(NOBODY))

    val contacts = DB.withConnection { implicit c =>
      SQL("select * from contact").as(contact *)
    }

    Ok(toJson(Map("contacts" -> contacts))).as("application/json")
  }

  def delete(id : Long) = Action {request =>
    val rows = DB.withConnection { implicit c =>
      SQL("delete from contact where id = {id}").on('id -> id).executeUpdate()
    }

    val socketId = request.headers.get("X-Pusher-Socket")

    // Send message through Pusher
    Logger.debug("Received delete action from " + socketId.getOrElse(NOBODY))
    val promise = Pusher().trigger(
      contactsChannel,
      newContactEvent,
      "{\"message\" : \"deleted " + rows + " rows\"}",
      socketId)

    Ok("{\"contacts\" : [{\"id\": " + id + "}]}").as("application/json")
  }
}
