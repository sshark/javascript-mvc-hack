package models

import java.util.Date

case class Contact(id : Long, name : String, email : String, age : Int, country : String, city : String, dateCreated : Date)
//case class Contact(id : Long, name : String, email : String, age : Int, country : String, city : String)