#Introduction
I wrote this trivial contact management to learn and understand contemporary Javascript frameworks. My objectives for this application are,

1. Persist new contact into the database.
2. Validate user input before submitting them to the server.
3. Edit contact from the contact list.
4. Share contacts with other users.

##Requirements
This application uses [Play for Scala][1]. Please refer to [Play Framework][2] website for more information.

The project dependencies are managed using npm and Bower. 

##Before you start
1. Node.js, npm and Bower are setup in the system.
2. After the project is cloned using `git`, `cd <project>/ui`
2. `npm install` to setup project 
3. `bower install` to download and setup project libraries
4. `sbt run` to start Play server

Register with [Pusher][4] to sync contacts between web browser instances. After an account is successful registered with Pusher, it will provide 3 pieces of information which must be added to `conf/application.conf` file. They are the application credentials and tokens.

    # Pusher configuration
    pusher.appId=<APP_ID>
    pusher.key=<APP_KEY>
    pusher.secret=<APP_SECRET>

##Demo

To begin please log on to,

    http://localhost:9000/

after you have started Play server using the server default settings.


  [1]: http://www.playframework.com/documentation/2.2.x/ScalaTodoList
  [2]: http://www.playframework.com
  [3]: http://backgridjs.com/
  [4]: http://pusher.com/
  [5]: http://localhost:9000

