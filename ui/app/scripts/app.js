/* global $,Pusher */

var addSocketIdToXHR;

// to satisfied jshint
var Contacts = window.Contacts = Ember.Application.create({
    LOG_TRANSITIONS: true,
    LOG_VIEW_LOOKUPS: true
});

Contacts.Store = DS.Store.extend({
    revision: 12
});

Contacts.Pusher = new Pusher('225c245e890197f08444');
Contacts.Pusher.connection.bind('connected', function () {
    addSocketIdToXHR(Contacts.Pusher.connection.socket_id);
    console.log("Added Pusher socket id ," + Contacts.Pusher.connection.socket_id + " to connection.");
});

// Add X-Pusher-Socket header so we can exclude the sender from their own actions
// More: [http://pusher.com/docs/server_api_guide/server_excluding_recipients]
function addSocketIdToXHR(socketId) {
    var _this = this;
    Ember.$.ajaxPrefilter(function(options, originalOptions, xhr) {
      return xhr.setRequestHeader('X-Pusher-Socket', socketId);
    });
}

/* Order and include as you please. */
require('scripts/controllers/*');
require('scripts/store');
require('scripts/models/*');
require('scripts/routes/*');
require('scripts/views/*');
require('scripts/router');

/* Not required unless jQuery UI is included
var btn = $.fn.button.noConflict(); // reverts $.fn.button to jqueryui btn
$.fn.btn = btn;
*/


/*
Ember.TextField.reopen({
  render : function() {
      this._super();
      Ember.run.scheduleOnce('afterRender', this, this.afterRenderEvent);
  },
  afterRenderEvent : function() {
      Ember.debug("afterRenderEvent => " + $().val());
      $().trigger("keyup");
  }
});
*/


$(document).ready(function() {
    $('#contact-form').show("drop", {
        direction : "up",
        complete: function() {
            $("#contacts-tableview").show("drop", {
                direction : "right"
            }, 800);
        }
    }, 1000);
});
