/* global $,Pusher,moment,Contacts */

var attr = DS.attr;

Contacts.Contact = DS.Model.extend({
    name : attr('string'),
    email : attr('string'),
    age : attr('number'),
    country : attr('string', {defaultValue : "Singapore"}),
    city : attr('string', {defaultValue : "Singapore"}),
    dateCreated : attr('date', {defaultValue : function() {new Date();}}),
    elaspedTime : function() {
        return moment(this.get('dateCreated')).startOf('time').fromNow();
    }.property('dateCreated')
});

Contacts.VerifiableContact = Ember.Object.extend(Ember.Validations.Mixin, Ember.Copyable, {
    /* copy is kept for future use. It is not tested. */
    copy : function(deep) {
        if (deep) {
            throw new Ember.Error("Deep copy is not supported.");
        }
        return Contacts.VerifiableContact.create({
            id : this.id,
            name : this.name,
            email : this.email,
            age : this.age,
            country : this.country,
            city : this.city,
            dateCreated : this.dateCreated
        });
    },
    id : null,
    name : null,
    email : null,
    age : null,
    country :"Singapore",
    city : "Singapore",
    dateCreated : null,
    validations : {
        name : {
            presence : {
                message : 'Name must be entered'
            }
        },
        email : {
            format: {
                with : /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/,
                message: 'Email must be entered and in a proper format'
            }
        },
        age : {
            numericality : {
                onlyInteger : true,
                greaterThanOrEqualTo : 18,
                lessThanOrEqualTo : 99,
                messages : {
                    greaterThanOrEqualTo : "Age must be above 17",
                    lessThanOrEqualTo : "Age must be below 100",
                    numericality : "Age must be entered"
                }
            }
        }
    }
});
