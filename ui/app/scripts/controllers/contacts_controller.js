/* global $,Contacts */

Contacts.ContactsController = Ember.ArrayController.extend({
    init : function() {
        this._super();
        var self = this;
        var channel = Contacts.Pusher.subscribe("contacts_channel");
        channel.bind("contact_new", function(data) {
            self.store.unloadAll(Contacts.Contact);
            self.store.find("contact").then(function(data) {
                self.set("content", data);
                Ember.debug("Contact list is updated.");
                $.notifyBar({
                    html: "Contact list updated."
                });
                self.transitionToRoute("contact");
            });
        });
    },
    sortProperties : ["name"],
    sortAscending : true,
    sortedContent: function() {
        return Ember.ArrayProxy.createWithMixins(Ember.SortableMixin, {
            content : this.content,
            sortProperties : this.get("sortProperties"),
            sortAscending : this.get("sortAscending")
        });
    }.property("content.@each", "sortProperties", "sortAscending")
});

Contacts.ContactsActionsView = Ember.View.extend({
    templateName : "contactsActions"
});
