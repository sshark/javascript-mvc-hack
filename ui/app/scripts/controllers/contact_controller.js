/* global $,Pusher,moment,Contacts */

var citiesForCountry;

Contacts.ContactEditController = Ember.ObjectController.extend({
    actions : {
        save : function() {
            this.get("model").save();
            this.transitionToRoute("contact");
        }
    }
});

Contacts.ContactController = Ember.ObjectController.extend({
    nameFailed : null,
    emailFailed : null,
    ageFailed : null,
    clearFailedMessages : function() {
        this.set('nameFailed', null);
        this.set('emailFailed', null);
        this.set('ageFailed', null);
    },
    contactKeys : ["id", "name", "age", "email", "country", "city", "dateCreated"],
    needs : "contacts",
    contactsController : Ember.computed.alias("controllers.contacts"),
    actions : {
        save : function() {
            var model = this.get("model");
            var self = this;

            if (model.get("id") != null) {
                model.validate().then(function() {
                    self.store.getById("contact", model.get("id")).setProperties(model.getProperties(self.contactKeys))
                       .save().then(function(contact) {
                           Ember.debug("Update " + contact.get("name") + " successful.");
                       }).catch(function(reason) {
                           Ember.error("Failed to update contact because " + reason.responseText);
                       });
                    self.set("model", Contacts.VerifiableContact.create({}));
                    self.clearFailedMessages();
                }, function() {
                    self.set('nameFailed', model.get("errors.name")[0]);
                    self.set('emailFailed', model.get("errors.email")[0]);
                    self.set('ageFailed', model.get("errors.age")[0]);
                    Ember.debug("Form validation because of " +
                        self.get('nameFailed') + ", " +
                        self.get('emailFailed') + ", " +
                        self.get('ageFailed'));
                });
            } else {
                model.validate().then(function() {
                    self.store.createRecord("contact", model.getProperties(self.contactKeys))
                        .save().then(function(contact) {
                            Ember.debug("Saved " + contact.get("name") + " with id " + contact.get("id"));
                        }).catch(function(reason) {
                            Ember.error("Failed to save contact because " + reason.responseText);
                        });
                    self.set("model", Contacts.VerifiableContact.create({}));
                    self.transitionToRoute("contact");
                    self.clearFailedMessages();
                }, function() {
                    self.set('nameFailed', model.get("errors.name")[0]);
                    self.set('emailFailed', model.get("errors.email")[0]);
                    self.set('ageFailed', model.get("errors.age")[0]);
                    Ember.debug("Form validation because of " +
                        self.get('nameFailed') + "," +
                        self.get('emailFailed') + "," +
                        self.get('ageFailed'));
                });
            }
        },
        clear : function() {
            this.set("model", Contacts.VerifiableContact.create({}));
        },
        edit : function(contact) {
            Ember.debug("Edit contact id " + contact.get("id"));
            this.transitionToRoute("contact.edit", Contacts.VerifiableContact.create(contact.getProperties(this.contactKeys)));
            $('.form-group input').trigger("keyup");
        },
        remove : function(contact) {
            Ember.debug("Delete contact id " + contact.get("id"));
            contact.destroyRecord();
        }
    },
    countrySelected : function() {
        var self = this;
        var country = this.get("country");
        if (country != null) {
            citiesForCountry(country, function(cities) {
                self.set("cities", cities);
                if (!Ember.isEmpty(cities)) {
                    self.set("city", cities[0]);
                }
            });
        } else {
            self.set("cities", []);
        }
    }.observes("model.country"),
    countries : ["Singapore", "Thailand", "Malaysia"],
    cities : []
});

function citiesForCountry(country, onSuccess, onError) {
    $.ajax({
        url: "/cities/" + country,
        type: "GET",
        async: true,
        success : onSuccess,
        error : onError
    });
}
