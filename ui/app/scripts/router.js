/* global Contacts */

Contacts.Router.map(function() {
    this.resource('contact', {path : "/:contact_id"}, function() {
        this.route('edit');
    });
});
