/* global Contacts */

Contacts.ContactRoute = Ember.Route.extend({
    setupController : function(controller, model) {
        Ember.debug("contact setup controller");
        controller.set('model', model);
        var self = this;
        this.store.find('contact').then(function(data) {
            self.controllerFor('contacts').set('model', data);
        });
    },
    model : function(params) {
        Ember.debug("contact model");
        return Contacts.VerifiableContact.create({});
    }
});

Contacts.IndexRoute = Ember.Route.extend({
    redirect : function() {
        this.transitionTo('contact');
    }
});
